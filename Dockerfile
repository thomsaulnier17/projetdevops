FROM openjdk:8-jdk-alpine
VOLUME /tmp
ONBUILD RUN mvn install
ONBUILD ADD /target/devops-0.0.1-SNAPSHOT.war app.war
ENTRYPOINT ["java","-jar","/app.war"]
